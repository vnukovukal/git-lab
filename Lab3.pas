program Lab3;
Const
    Size=45;
    Year1=5;
    Year2=2013;
    Months=12;
    Days=31;
Type Anketa=record
    Year:integer;
    Month:byte;
    Day:byte;
end;
Type Arr=array [1..Size] of Anketa;
Procedure AddWorkers (var _Mass : Arr);
var
  _i,_temp:integer;
begin
    _temp:=random(40)+5;
    for _i:=1 to _temp do
    begin
        with _Mass[_i] do
        begin
            Year:=Random(Year1)+Year2;
            Month:=Random(Months);
            Day:=Random(Days);
            WriteLn(_i,'    ',Day,'.',Month,'.',Year);
        end;
    end;
end;
Procedure FindMin(var Mass:Arr);
var
  iTemp,i,MinYear:integer;
  MinDay,MinMonth:byte;
begin
   MinYear:=2016;
   MinDay:=32;
   MinMonth:=13;
   for i:=1 to Size do
   begin
      if Mass[i].Year<MinYear then
      MinYear:=Mass[i].Year;
   end;
   for i:=1 to Size do
   begin
      if Mass[i].Year=MinYear then
      begin
        if Mass[i].Month<MinMonth then
        MinMonth:=Mass[i].Month;
      end;
   end;
   for i:=1 to Size do
   begin
      if Mass[i].Year=MinYear then
      begin
        if Mass[i].Month=MinMonth then
        begin
           if Mass[i].Day<MinDay then
           MinDay:=Mass[i].Day;
        end;
      end;
   end;
   writeln ('Minimals');
   writeln ('    ',MinDay,'.',MinMonth,'.',MinYear);
end;

var Mass : Arr;
begin
    Randomize;
    AddWorkers (Mass);
    FindMin(Mass);
    Readln;
end.
