program Lab32;
Const
    Size=45;
    Year1=5;
    Year2=2012;
    Months=11;
    Days=30;
Type Anketa=record
    Year:integer;
    Month:byte;
    Day:byte;
end;
Type Arr=array [1..Size] of Anketa;
Procedure CreateRecord (var _Mass : Arr;var _temp:integer);
var
  _i:integer;
begin
    _temp:=random(40)+5;
    for _i:=1 to _temp do
    begin
        with _Mass[_i] do
        begin
            Year:=Random(Year1)+Year2+1;
            Month:=Random(Months)+1;
            Day:=Random(Days)+1;
            WriteLn(_i,'    ',Day,'.',Month,'.',Year);
        end;
    end;
end;
Procedure MinData(var Mass:Arr;var _temp:integer);
var
  iTemp,i,MinYear:integer;
  MinDay,MinMonth:byte;
begin
   MinYear:=Mass[1].Year;
   MinDay:=Mass[1].Day;
   MinMonth:=Mass[1].Month;
   for i:=1 to _temp do
   begin
      if Mass[i].Year<MinYear then
      MinYear:=Mass[i].Year;
   end;
   for i:=1 to _temp do
   begin
      if Mass[i].Year=MinYear then
      begin
        if Mass[i].Month<MinMonth then
        MinMonth:=Mass[i].Month;
       end;
   end;
   for i:=1 to _temp do
   begin
      if Mass[i].Year=MinYear then
      begin
        if Mass[i].Month=MinMonth then
        begin
           if Mass[i].Day<MinDay then
           MinDay:=Mass[i].Day;
        end;
      end;
   end;
   writeln ('Minimals');
   writeln ('    ',MinDay,'.',MinMonth,'.',MinYear);
end;
Procedure MaxData(var Mass:Arr;var _temp:integer);
var
  i,MaxYear:integer;
  MaxDay,MaxMonth:byte;
begin
   MaxYear:=Mass[1].Year;
   MaxDay:=Mass[1].Day;
   MaxMonth:=Mass[1].Month;
   for i:=1 to _temp do
   begin
      if Mass[i].Year>MaxYear then
      MaxYear:=Mass[i].Year;
   end;
   for i:=1 to _temp do
   begin
      if Mass[i].Year=MaxYear then
      begin
        if Mass[i].Month>MaxMonth then
        MaxMonth:=Mass[i].Month;
       end;
   end;
   for i:=1 to _temp do
   begin
      if Mass[i].Year=MaxYear then
      begin
        if Mass[i].Month=MaxMonth then
        begin
           if Mass[i].Day>MaxDay then
           MaxDay:=Mass[i].Day;
        end;
      end;
   end;
   writeln ('Maximals');
   writeln ('    ',MaxDay,'.',MaxMonth,'.',MaxYear);
end;
procedure EnterData(var _temp:anketa);
  var counter:integer;
begin
   counter:=0;
   repeat
   writeln('Enter your date');
   readln(_temp.year);
   readln(_temp.month);
   readln(_temp.day);
   if  (_temp.year<2013) or (_temp.year>2018) then
   begin
      writeln ('Tne year was entered incorrectly');
      counter:=1;
   end;
   if  (_temp.month<1) or (_temp.month>12) then
   begin
      writeln ('Tne month was entered incorrectly');
      counter:=1;
   end;
   if  (_temp.day<1) or (_temp.day>31) then
   begin
      writeln ('Tne day was entered incorrectly');
      counter:=1;
   end;
   until counter=0;
end;
Function Midle (var Mass:Arr;var _temp:integer;var _Data:Anketa):real;
var
  i:integer;
  tempDay,tempMonth,tempYear:integer;
  counter,staj:real;
begin
   for i:=1 to _temp do
   begin
      if Mass[i].Day>_Data.Day then
      begin
         Mass[i].Month:=Mass[i].Month+1;
      end;
      if Mass[i].Month>_Data.Month then
      begin
         Mass[i].Year:=Mass[i].Year+1;
      end;
      if Mass[i].Year<_Data.Year then
      begin
         tempYear:=_Data.Year-Mass[i].Year;
         counter:=counter+1;
      end;
   end;
   staj:=tempYear/counter;
   Midle:=staj;
end;

var Massive : Arr;
    temp : integer;
    Data : Anketa;
    stag:real;
begin
    Randomize;
    CreateRecord (Massive,temp);
    MaxData(Massive,temp);
    MinData(Massive,temp);
    EnterData(Data);
    stag:=Midle (Massive,temp,Data);
    WriteLn('Midle date' ,stag:0:3);
    Readln;
end.  
